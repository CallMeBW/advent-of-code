from typing import Iterable

def parse_ints(filepath: str) -> Iterable[int]:
  with open(filepath) as f:
    return map(int, f.read().splitlines())

def genSlidingWindows(seq: list[int], n: int) -> Iterable[tuple[int, ...]]:
  """ generates tuples of size n from the input seq, with sliding step size of 1"""
  return zip(*(seq[i:] for i in range(n)))

def count_increases(ints: list[int], win_size: int) -> int:
  """ counts how many times the sum in a sliding window increases """
  sums = list(map(sum, genSlidingWindows(ints, win_size)))
  return sum(n < m for n, m in zip(sums, sums[1:]))

if __name__ == "__main__":
    ints = list(parse_ints('day01/input.txt'))
    print(count_increases(ints, 1))
    print(count_increases(ints, 3))