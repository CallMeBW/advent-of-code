from collections import namedtuple

def parse_commands(filepath: str) -> list[tuple[str, int]]:
  with open(filepath) as f:
    lines = [line.split(' ') for line in f.read().splitlines()]
    return [(cmd, int(dist)) for (cmd, dist) in lines]

Vec = namedtuple('Location', ('x', 'y', 'aim'))

dir_map_1 = {
  'forward': lambda _: Vec(1, 0, 0),
  'down': lambda _: Vec(0, 1, 0),
  'up': lambda _: Vec(0, -1, 0)
}
dir_map_2 = {
  'forward': lambda loc: Vec(1, loc.aim, 0),
  'down': lambda _: Vec(0, 0, 1),
  'up': lambda _: Vec(0, 0, -1)
}

def get_target_position(dir_map, commands):
  """ returns the final position """
  loc = Vec(0,0, 0)
  for (cmd, x) in commands:
      move_vec = dir_map[cmd](loc)
      loc = Vec(loc.x + x * move_vec.x, loc.y + x * move_vec.y, loc.aim + x * move_vec.aim)
  return loc

def target_to_answer(location: Vec):
  """ transforms target location to desired scalar value """
  return location.x * location.y

if __name__ == "__main__":
    commands = list(parse_commands('day02/input.txt'))
    print(target_to_answer(get_target_position(dir_map_1, commands)))
    print(target_to_answer(get_target_position(dir_map_2, commands)))
