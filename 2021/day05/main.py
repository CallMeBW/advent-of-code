from typing import DefaultDict
import numpy as np
from numpy.core.fromnumeric import shape
from collections import defaultdict


def parseInput(filePath: str) -> list[tuple[np.ndarray, np.ndarray]]:
    """ returns (input_sequence, boards), where boards is a 3D array """
    with open(filePath) as f:
        num_strs = [l.split('->') for l in f.read().splitlines()]
        nums = [(np.fromstring(left, dtype=int, sep=','), np.fromstring(right, dtype=int, sep=',')) for (left, right) in num_strs]
        return nums
        # boards = np.genfromtxt(f, dtype=int)
        # return input_seq, boards.reshape((len(boards)//SIZE, SIZE, SIZE))



if __name__ == "__main__":
    inputs = parseInput('day05/input.txt')
    counts = defaultdict(int)
    for left, right in inputs:
      common_axis = left == right
      if not any(common_axis):
        print('no common')
        continue
      range_noncommon = np.arange(min(left[~common_axis], right[~common_axis]), max(left[~common_axis], right[~common_axis])+1, dtype=int)
      common_val = left[common_axis]
      [target_axis] = np.where(common_axis)[0]
      indices = [(val, common_val) if target_axis == 0 else (common_val, val) for val in range_noncommon]
      for dict_index in indices:
        counts[dict_index] += 1
    print(counts)
      
    