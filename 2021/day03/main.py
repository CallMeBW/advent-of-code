from typing import Iterable


def parse_lines(filepath: str) -> list[str]:
  with open(filepath) as f:
    return f.read().splitlines()


def part1():
    lines = parse_lines('day03/input.txt')
    num_size = len(lines)
    num_nums = len(lines[0])
    nums = []
    for col in range(num_nums):
      nums.append("")
      for row in range(num_size):
          nums[col] += lines[row][col]
    sums = [num.count("1") for num in nums]
    mcb = [int(summ > num_size//2) for summ in sums]
    lcb = [int(not b) for b in mcb]
    gamma = int(''.join(map(str, mcb)), base=2)
    epsilon = int(''.join(map(str, lcb)), base=2)
    print(gamma)
    print(epsilon)
    print(gamma*epsilon)


def oxygen_mask(digits: list[str]):
      most_common = '1' if digits.count('1') >= (len(digits)//2) else '0'
      oxygen_mask = [i for (i,digit) in enumerate(digits) if digit == most_common]
      #c02_mask = [not bool for bool in range(len(oxy))]
      return oxygen_mask


def part2():
    lines = parse_lines('day03/input.txt')
    num_size = len(lines)
    num_nums = len(lines[0])
    nums = []
    for col in range(num_nums):
      nums.append("")
      for row in range(num_size):
          nums[col] += lines[row][col]
    for digitArray in nums:
      mask = oxygen_mask(digitArray)
      digitArray = [d for (i, d) in enumerate(digitArray) if i in mask]
      if 

    

if __name__ == "__main__":
  #part1()
  part2()