#! /bin/bash
[ -z "$1" ] && echo "Please provide day as first argument." && exit 1
day="$1"
if [ "$day" -lt "10" ]; then
    day="0$day"
fi
mkdir "day$day" || exit 1
curl -s "https://adventofcode.com/2021/day/${1}/input" --cookie \
  "session=53616c7465645f5f6dc73dc9d4a0c4da4b90c3160de510c91e8d6c941f92f6e56a4d4aab8fba32256f81caef6d96b31a" \
  > "day$day/input.txt"