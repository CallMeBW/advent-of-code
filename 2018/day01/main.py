import itertools
from pathlib import Path
from typing import Optional

def line_to_num(line: str) -> int:
    if line[0] == "+":
        return int(line[1:])
    return int(line)

def first_repeating_freq(frequency_changes: list[int]) -> Optional[int]:
    acc = 0
    past_acc = set([acc])
    for freq_change in itertools.cycle(frequency_changes):
        if (acc := acc + freq_change) in past_acc:
            return acc
        past_acc.add(acc)

def main():
    frequency_changes = Path('day01/input.txt').read_text().split('\n')
    frequency_changes = list(map(line_to_num, frequency_changes))
    print(sum(frequency_changes)) # part 1
    print(first_repeating_freq(frequency_changes)) # part 2

if __name__ == "__main__":
    main()