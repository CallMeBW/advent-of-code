#!/usr/bin/env python3

from pathlib import Path

INPUT_URL = 'https://adventofcode.com/2018/day/1/input'
CURRENT_DIR = Path('.')
DEFAULT_FILES = [
  'input.txt',
  'main.py'
]

def create_next_directory(day_of_month: int) -> Path:
    leading_zero = "" if day_of_month >= 10 else "0"
    new_folder_name = f"day{leading_zero}{str(day_of_month)}"
    new_directory = CURRENT_DIR / new_folder_name
    assert not new_directory.exists()
    new_directory.mkdir()
    return new_directory

def get_latest_day() -> int:
    existing_folders = [file for file in CURRENT_DIR.iterdir()
                        if file.is_dir() and file.name.startswith('day')]
    existing_folders.sort()
    if not existing_folders:
      return 0
    latest_day = existing_folders[-1].name[3:]
    return int(latest_day)

def create_default_files(directory: Path):
    for file_name in DEFAULT_FILES:
      with open(directory / file_name, 'w'):
        pass # simply create the file

def main():
    latest_day = get_latest_day()
    new_directory = create_next_directory(latest_day + 1)
    create_default_files(new_directory)


if __name__ == "__main__":
    main()