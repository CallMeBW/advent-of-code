from functools import reduce
from operator import mul
from pathlib import Path
from typing import Counter, Optional

def number_of_relevant_counts(counter: Counter):
    relevant_numbers = [2,3]
    counts = counter.values()
    return [int(number in counts) for number in relevant_numbers]

def calculate_checksum(lines: list[str]):
    counts_per_line = map(Counter, lines)
    relevant_per_line = map(number_of_relevant_counts, counts_per_line)
    counts_per_relevant_number = map(sum, zip(*relevant_per_line))
    return reduce(mul, counts_per_relevant_number)

def list_duplicates(seq):
    seen = set()
    return set(x for x in seq if x in seen or seen.add(x))

def find_matching_susbstring(lines: list[str]) -> Optional[str]:
    for i in range(len(lines[0])):
      lines_modified = [line[:i] + line[i+1:] for line in lines]
      if duplicates := list_duplicates(lines_modified):
        return duplicates.pop()

def main():
    lines = Path('day02/input.txt').read_text().split('\n')
    print(calculate_checksum(lines)) # part 1
    print(find_matching_susbstring(lines)) # part 2

if __name__ == "__main__":
    main()