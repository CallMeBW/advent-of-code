import itertools
import re
from pathlib import Path
from typing import Counter, NamedTuple

REGEX_PATTERN = r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)"

class Claim(NamedTuple):
    id_: int
    offset_x: int
    offset_y: int
    width: int
    height: int

def line_to_claim(line: str) -> Claim:
  assert (match := re.match(REGEX_PATTERN, string=line)), f"error: {line}"
  return Claim(*map(int, match.groups()))

def calc_occupied_coords(claim: Claim) -> list[tuple[int, int]]:
  return [(x + claim.offset_x, y + claim.offset_y)
          for x in range(claim.width)
          for y in range(claim.height)]

def get_total_overlaps(occupied_coords_counts: Counter):
  return sum(1 for val in occupied_coords_counts.values() if val > 1)

def main():
    lines = Path('day03/input.txt').read_text().split('\n')
    claims = list(map(line_to_claim, lines))
    occupied_indices_list = list(map(calc_occupied_coords, claims))
    occupied_coords_counts = Counter(itertools.chain(*occupied_indices_list))
    print(get_total_overlaps(occupied_coords_counts)) # part 1

    for claim, occupied_indices in zip(claims, occupied_indices_list):
      if all(occupied_coords_counts[index] == 1 for index in occupied_indices):
        print(claim.id_) # part 2
        break

if __name__ == "__main__":
    main()