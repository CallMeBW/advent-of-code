import re
from collections import defaultdict
from dataclasses import dataclass, field
from datetime import datetime
from itertools import chain
from pathlib import Path
from pprint import pformat
from typing import Counter


MSG_REGEX = r"\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (.*)"
GUARD_REGEX = r".*?Guard #(\d+).*"

def to_datetime_minute(string: str) -> int:
  return datetime.strptime(string, '%Y-%m-%d %H:%M').minute

GUARD_ID = str
SLEEP_WAKE_TIMES = list[tuple[datetime, datetime]]

@dataclass
class Guard:
  _id: int
  sleep_minutes: list[tuple[int,int]] = field(default_factory=list)
  latest_sleep_time: int|None = None

  def track_event(self, sleep_wake_minute: int):
    if self.latest_sleep_time is None:
      self.latest_sleep_time = sleep_wake_minute
    else:
      self.sleep_minutes.append((self.latest_sleep_time, sleep_wake_minute))
      self.latest_sleep_time = None
  
  def longest_sleep(self) -> int:
    times = [wake - sleep for sleep, wake in self.sleep_minutes]
    return sum(times)

  def most_frequent_sleep_minute(self) -> tuple[int, int]:
    sleep_mins = chain.from_iterable(list(range(a,z)) for a,z in self.sleep_minutes)
    [most_common_min] = Counter(sleep_mins).most_common(1)
    return most_common_min # returns the most common minute and how often it appears


  def __repr__(self) -> str:
    return pformat(self.sleep_minutes)

def main():
  lines = Path('day04/input.txt').read_text().split('\n')
  lines.sort()
  current_guard = 0
  guard_id_to_info = defaultdict(lambda: Guard(current_guard))
  for line in lines:
    if 'Guard' in line:
      current_guard = int(re.findall(GUARD_REGEX, line)[0])
    else:
      datetime_str, _ = re.findall(MSG_REGEX, line)[0]
      guard_id_to_info[current_guard].track_event(to_datetime_minute(datetime_str))
  guard1 = max(guard_id_to_info.values(), key=lambda guardinfo: guardinfo.longest_sleep())
  guard2 = max(guard_id_to_info.values(), key=lambda guardinfo: guardinfo.most_frequent_sleep_minute()[1])
  print(f"Guard #{guard1._id} sleeps the most -> solution: {guard1.most_frequent_sleep_minute()[0]*guard1._id}")
  print(f"Guard #{guard1._id} sleeps more on a certain minute -> solution: {guard2.most_frequent_sleep_minute()[0]*guard2._id}")
  

  

if __name__ == "__main__":
    main()