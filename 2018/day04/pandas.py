import re, pandas as pd
from pathlib import Path
from datetime import datetime
from functools import reduce

def to_datetime(string: str) -> datetime:
  return datetime.strptime(string, '%Y-%m-%d %H:%M')

def aggegate_by_date(series):
       logs = series.reset_index(drop=True)
       sleep_wakes = list(zip(logs[0::2], logs[1::2]))
       times_asleep: list[int] = [log2.minute - log1.minute for log1, log2 in sleep_wakes]
       return sum(times_asleep)

LOG_REGEX = r"\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (.*)"
lines = Path('day04/test_input.txt').read_text().split('\n')
#lines = Path('day04/input.txt').read_text().split('\n')
logs = [re.findall(LOG_REGEX, l)[0] for l in lines]
logs = [(to_datetime(datestr), msg) for datestr, msg in logs]
df = pd.DataFrame(logs, columns=['timestamp', 'msg'])
df = df.sort_values(by=['timestamp']).reset_index(drop=True)
shift_rows = df['msg'].str.contains('begins shift')
df['guard'] = None
df['guard'].loc[shift_rows] = df['msg'].loc[shift_rows].str\
                                  .findall(r"Guard #(\d+).*")\
                                  .apply(lambda x: x[0])
current_guard = None
for idx, guard in enumerate(df['guard']):
    guard_val = df['guard'].loc[idx]
    if guard_val == None:
      df['guard'].loc[idx] = current_guard
    else:
      current_guard = guard_val
      df['guard'].loc[idx] = None
print(df.groupby('guard').agg({'timestamp': [aggegate_by_date]}))