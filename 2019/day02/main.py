with open('day02/input.txt') as f:
  code = [int(num) for num in f.read().split(',')]

HALT = 99

def add(instr_idx, source_code):
  output_idx = source_code[instr_idx + 3]
  source_code[output_idx] = source_code[instr_idx + 1] + source_code[instr_idx + 2]

def multiply(instr_idx, source_code):
  output_idx = source_code[instr_idx + 3]
  source_code[output_idx] = source_code[instr_idx + 1] + source_code[instr_idx + 2]

opcode_to_op = {
  1: add,
  2: multiply
}

#code[1] = 12
#code[2] = 2

instr_idx = 0

while (opcode := code[instr_idx]) != HALT:
  opcode_to_op[opcode](instr_idx, code)
  instr_idx += 1

print(code)


