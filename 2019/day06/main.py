from dataclasses import dataclass

def parse_input():
  with open('day06/input.txt') as f:
    return [(line[:3],line[4:]) for line in f.read().splitlines()]

@dataclass
class Node:
  name: str = ""
  children: list['Node'] = []

  def __hash__(self):
    return hash(self.name)



if __name__ == "__main__":
    orbits = parse_input()
    without_children: set[str] = set([child for parent,child in orbits])
    parent_to_node = 

    for parent, child in orbits:
      if parent in without_children:
        without_children.remove(parent)
      node = Node(name=parent)
      if node in nodes:
        nodes
      

