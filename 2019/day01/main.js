import { readFileSync } from 'fs';

const fuel = mass => Math.max(0, Math.floor(mass / 3) - 2);
const sum = array => array.reduce((a, b) => a + b, 0);

const calcFuelRecursive = mass => {
  const fuelSum = fuel(mass);
  if (fuelSum <= 0) return 0;
  return fuelSum + calcFuelRecursive(fuelSum);
}

const lines = readFileSync('day01/input.txt').toString().split("\n")
console.log(sum(lines.map(fuel)))
console.log(sum(lines.map(calcFuelRecursive)))
