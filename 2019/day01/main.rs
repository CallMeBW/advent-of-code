use std::{
  fs::File,
  io::{prelude::*, BufReader}
};

fn read_lines(filename: &str) -> Vec<String> {
  BufReader::new(File::open(filename).unwrap())
      .lines().map(|l| l.unwrap()).collect()
}

fn fuel(mass: usize) -> usize {
  let f: usize = mass / 3;
  if f <= 2 {
    return 0;
  }
  return f - 2;
}

fn fuel_recursive(mass: usize) -> usize {
  let fuel_sum = fuel(mass);
  if fuel_sum == 0 {
    return 0;
  }
  return fuel_sum + fuel_recursive(fuel_sum);
}

fn main() {
  let rows = read_lines("day01/input.txt");
  let fuel_sum: usize = rows.iter()
                                .map(|x| x.parse::<usize>().unwrap())
                                .map(|x| fuel(x))
                                .sum();
  let fuel_sum_rec: usize = rows.iter()
                                  .map(|x| x.parse::<usize>().unwrap())
                                  .map(|x| fuel_recursive(x))
                                  .sum();
  println!("{:?}", fuel_sum);
  println!("{:?}", fuel_sum_rec);
}