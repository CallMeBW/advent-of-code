with open('day01/input.txt') as f:
  lines = f.read().splitlines()

def fuel(mass):
  return max(0, (mass // 3) - 2)

def fuelRecursive(mass):
  fuelSum = fuel(mass)
  if fuelSum == 0:
    return 0
  return fuelSum + fuelRecursive(fuelSum)

print(sum(fuel(int(x)) for x in lines))
print(sum(fuelRecursive(int(x)) for x in lines))
