import java.io.File
import kotlin.math.*

var masses = File("day01/input.txt").bufferedReader()
              .readLines()
              .map(String::toInt)

fun fuel(mass: Int): Int =
  max(0, (floor((mass/3).toDouble()) - 2).toInt())

fun fuelRecursive(mass: Int): Int {
  var fuelSum = fuel(mass)
  if (fuelSum == 0)
    return 0
  return fuelSum + fuelRecursive(fuelSum)
}

println(masses.map(::fuel).sum())
println(masses.map(::fuelRecursive).sum())