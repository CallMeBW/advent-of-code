from collections import Counter

def parse_input():
  with open('day04/input.txt') as f:
    bounds = f.read().split('\n')[0].split('-')
    return list(map(int, bounds))


def fulfills(some_int):
  string = f"{some_int}"
  contains_double = 2 in Counter(string).values()
  never_decreasing = all([int(string[i]) <= int(string[i+1]) for i in range(5)])
  return never_decreasing and contains_double

if __name__ == "__main__":
    [lower, upper] = parse_input()
    all_candidates = range(lower, upper+1)
    passwords = filter(fulfills, all_candidates)
    print(len(list(passwords)))