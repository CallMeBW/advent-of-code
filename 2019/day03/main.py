Path = list[str]
WayPoint = tuple[int, int] # x, y

# [[R1, U12, L2], [...], ...]
def parseInput() -> list[Path]:
  with open('day03/input.txt') as f:
    lines = f.read().splitlines()
  return [line.split(',') for line in lines]

dir_map = {
  'R': (1, 0),
  'L': (-1, 0),
  'U': (0, 1),
  'D': (0, -1),
}


def waypoints(path: Path):
  waypoints: set[WayPoint] = set()
  wp_to_stepcount = {}
  current_waypoint: WayPoint = (0, 0)
  stepcount = 0
  for command in path:
    direction, dist = [command[0], int(command[1:])]
    dirX, dirY = dir_map[direction]
    x, y = current_waypoint
    new_wp = x, y
    for i in range(1, dist + 1):
      stepcount += 1
      new_wp = (x+i*dirX, y+i*dirY)
      waypoints.add(new_wp)
      if not new_wp in wp_to_stepcount:
        wp_to_stepcount[new_wp] = stepcount
    current_waypoint = new_wp
  return waypoints, wp_to_stepcount

def dist(point: WayPoint):
  return abs(point[0]) + abs(point[1])

def stepCount(point: WayPoint, stepDict1, stepDict2):
  return stepDict1[point] + stepDict2[point]

if __name__ == "__main__":
    [p1, p2] = parseInput()
    wp1, stepcounts1 = waypoints(p1)
    wp2, stepcounts2 = waypoints(p2)
    intersections = wp1.intersection(wp2)
    min_dist = min(map(dist, intersections))
    min_steps = min([stepCount(wp, stepcounts1, stepcounts2) for wp in intersections])
    print(min_dist)
    print(min_steps)