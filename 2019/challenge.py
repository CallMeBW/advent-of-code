# https://ondras.github.io/dobble/
# Specs:
#  - each card has 5 different symbols
#  - each two cards of the deck have exactly one symbol in common
# 1. Given a single card, check if the card is valid.
# 2. Given a list of cards, check if the cards are a valid subset of a deck.
Card = list[int]
cards: list[Card] = [
  [1,2,3,4,5],
  [6,7,8,9,10],
  [5,6,7,8,9],
  [1,1,2,3,4],
  [4,8,11,12,13],
]

def is_valid(card: Card) -> bool:
  return len(set(card)) == 5

def is_pair_valid(card1: Card, card2: Card) -> bool:
  common_symbols = set(card1).intersection(set(card2))
  return len(common_symbols) == 1

def is_valid_deck(cards: list[Card]) -> bool:
  if not all(map(is_valid, cards)):
    return False
  for i in range(len(cards)-1):
    for j in range(i+1,len(cards)):
      if not is_pair_valid(cards[i], cards[j]):
        return False
  return True

if __name__ == "__main__":
    print(is_valid_deck([cards[0],cards[2],cards[4]]))