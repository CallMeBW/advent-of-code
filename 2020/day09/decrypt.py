import sys
from itertools import combinations, count
from typing import Iterable, Optional

nums = list(map(int, sys.stdin.readlines()))

def sums_up(seq: Iterable[int], target: int) -> bool:
    return any(sum(c)==target for c in combinations(seq, 2))

def find_weakness(cypher: int) -> Optional[int]:
    return next((nums[i] for i in range(cypher,len(nums))
        if (not sums_up(nums[i-cypher:i], nums[i]))), None)

def find_exploit(window_len: int, weakness: int) -> Optional[int]:
    ranges = [nums[i:i+window_len] for i in range(0, len(nums)-window_len)]
    return next((min(r) + max(r) for r in ranges if sum(r) == weakness), None)

if __name__ == "__main__":
    weakness = find_weakness(cypher=25)
    exploits_generator = (find_exploit(win_size, weakness) for win_size in count(start=2))

    print(weakness) # part 1
    print(next(e for e in exploits_generator if e is not None)) # part 2