import sys
from typing import List, Tuple

Command = Tuple[str, int] # ("acc", -5)
Instruction = Tuple[int, int] # (incr, acc)
Result = Tuple[int, bool] # (return value, terminated_successfully)

def parse(line: str) -> Command:
    cmd, val = line.split(" ")
    return cmd, int(val)

def compile_(command: Command) -> Instruction:
    cmd, val = command
    return (val if cmd == "jmp" else 1, val if cmd == "acc" else 0)

def run(code: List[Instruction], executed_set=set(), start=0) -> Result:
    if start >= len(code) or start in executed_set:
        return 0, start == len(code)
    incr, acc = code[start]
    result, success = run(code, executed_set | set([start]), start+incr)
    return result + acc, success

def swap_command(command: Command):
    cmd, val = command
    return ("jmp" if cmd == "nop" else "nop", val)


if __name__ == "__main__":
    commands = [parse(l) for l in sys.stdin.read().splitlines()]
    compiled_code = [compile_(c) for c in commands]
    (result, success) = run(compiled_code)
    print(result) # part 1

    swap_indices = (i for i, cmd in enumerate(commands) if cmd[0] in ["jmp", "nop"])
    while not success:
        i = next(swap_indices)
        commands[i] = swap_command(commands[i])
        (result, success) = run([compile_(c) for c in commands])
        commands[i] = swap_command(commands[i])
    print(result) # part 2