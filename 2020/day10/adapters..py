import sys
from functools import reduce
from typing import Dict

nums = sorted(map(int, sys.stdin.readlines()))
joltages = [0] + nums + [nums[-1]+3]

def count_skips(acc: Dict[int, int], curr_index: int):
    joltage = joltages[curr_index]
    last_joltage = joltages[curr_index-1]
    diff = joltage - last_joltage
    acc[diff] += 1
    return acc

if __name__ == "__main__":
    skips = reduce(count_skips, range(1,len(joltages)), {1:0,3:0})
    print(skips[1]*skips[3]) # part 1