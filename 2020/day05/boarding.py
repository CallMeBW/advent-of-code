import sys
from typing import Iterable, List, Set, Tuple
from itertools import groupby

rows = sys.stdin.read().splitlines()
BoardingPass = Tuple[int, int] # row, col

binary_codes = {
    "R": "1",
    "B": "1",
    "L": "0",
    "F": "0",
}

def seat_id(row: int, col: int) -> int:
    return (row * 8) + col

def parse_boarding_pass(line: str) -> BoardingPass:
    for code, repl in binary_codes.items():
        line = line.replace(code, repl)
    return (int(line[:7], 2), int(line[7:], 2))


def get_vacant_seats(row: Iterable[int]) -> Set[int]:
    return set(range(7)) - set(row)


def vacant_seats_by_row(sorted_passes: List[BoardingPass]) -> Tuple[int, Set[int]]:
    get_row = lambda pazz: pazz[0]
    get_col = lambda pazz: pazz[1]
    return [((row, get_vacant_seats(map(get_col, passes))))
            for row, passes in groupby(sorted_passes, get_row)]

if __name__ == "__main__":
    passes = sorted(map(parse_boarding_pass, rows))
    occupation = vacant_seats_by_row(passes)
    [my_pass] = [(row, free_seats.pop())
                 for row, free_seats in occupation if len(free_seats) == 1]

    print(seat_id(*passes[-1])) # part 1
    print(seat_id(*my_pass))    # part 2