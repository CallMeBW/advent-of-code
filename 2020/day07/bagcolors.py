import re
import sys
from functools import reduce
from typing import Dict, List, Set, Tuple

lines = sys.stdin.read().splitlines()
line_pattern = r"^(\w+\s\w+) bags contain (.*)$"
check_empty_pattern = r"(no other bags|(?P<contents>.*))"
content_pattern = r"(\d)\s(\w+\s\w+) bags?"

Bag = str # "muted black"
RuleSet = Dict[Bag, Set[Tuple[int, Bag]]] # a Rule maps bags to their contents

def parse_rule(raw_rule) -> RuleSet:
    bag, rule = re.match(line_pattern, raw_rule).groups()
    empty_check_matches = re.match(check_empty_pattern, rule)
    allowed_content = set()
    if empty_check_matches.group("contents"):
        content = empty_check_matches.group("contents")
        content_matches = re.findall(content_pattern, content)
        allowed_content = {(int(num), _bag) for num, _bag in content_matches}
    return (bag, allowed_content)

def find_possible_wrappers(content: Bag, rules: RuleSet) -> Set[Bag]:
    return set(wrapping_bag for wrapping_bag, allowed_contents in rules.items()
               if content in map(lambda x: x[1], allowed_contents))

def count_bags(bag: Bag, rules: RuleSet) -> int:
    if not (contents := rules[bag]):
        return 1
    return 1+(sum([num*count_bags(inner_bag, rules)
            for num, inner_bag in contents]))

def count_wrappers(bag: Bag, rules: RuleSet):
    solutions = find_possible_wrappers(search, rules)
    prev_len = 0
    while prev_len < len(solutions):
        prev_len = len(solutions)
        items = list(solutions)
        for wrapper in items:
            new_ones = find_possible_wrappers(wrapper, rules)
            solutions.update(new_ones)
    return len(solutions)

if __name__ == "__main__":
    rules = dict(map(parse_rule, lines))
    search = "shiny gold"
    print(count_wrappers(search, rules)) # part 1
    print(count_bags(search, rules)-1) # part 2