import sys, math

rows = sys.stdin.read().splitlines()
num_trees = [sum(
            rows[i * sy][i * sx % len(rows[0])] == '#'
            for i in range(len(rows) // sy))
        for sx, sy in ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))]

print(num_trees[1])         # part 1
print(math.prod(num_trees)) # part 2