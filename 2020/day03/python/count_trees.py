import sys
from math import prod

lines = sys.stdin.read().splitlines()
map_width = len(lines[0])


def is_tree_at(row, col):
    return lines[row][col % map_width] == '#'


def count_trees(slope_x, slope_y):
    return sum(is_tree_at(i * slope_y, i * slope_x)
               for i in range(len(lines) // slope_y))


if __name__ == "__main__":
    slopes = ((1, 1), (3, 1), (5, 1), (7, 1), (1, 2))
    num_trees = [count_trees(*s) for s in slopes]
    print(num_trees[1])     # part 1
    print(prod(num_trees))  # part 2