use std::{
    fs::File,
    io::{prelude::*, BufReader}
};

fn is_tree(rows: &Vec<String>, row: usize, col: usize) -> bool {
    let width = rows[0].len();
    (&rows[row]).chars().nth(col % width).unwrap() == '#'
}

fn count_trees(slope: &(usize, usize), rows: &Vec<String>) -> usize{
    let (sx, sy) = slope;
    (0..(rows.len() / sy))
        .map(|i| is_tree(&rows, i * sy, i * sx))
        .filter(|c| *c)
        .count()
}

fn read_lines(filename: &str) -> Vec<String> {
    BufReader::new(File::open(filename).unwrap())
        .lines().map(|l| l.unwrap()).collect()
}

fn main() {
    let rows = read_lines("src/map.txt");
    let slopes = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let p1: usize = count_trees(&slopes[1], &rows);
    let p2: usize = slopes.iter()
                        .map(|s| count_trees(&s, &rows))
                        .product();
    println!("{}", p1); // part 1
    println!("{}", p2); // part 2
}