import sys
import re
from itertools import starmap

def read_input():
    lines = sys.stdin.readlines()
    regex = r'^(?P<min>\d+)-(?P<max>\d+) (?P<char>\w): (?P<pw>\w*)$'
    capt_groups = (('min', int), ('max', int), ('char', str), ('pw', str))
    parse_to_tuple = lambda m: tuple(cast(m.group(name)) for name, cast in capt_groups)
    return [parse_to_tuple(re.search(regex, l)) for l in lines]

def is_pw_valid(min, max, char, pw):
    char_count = pw.count(char)
    return char_count >= min and char_count <= max

def is_pw_valid_v2(min, max, char, pw):
    is_char_at_i = lambda i: len(pw) >= i and pw[i-1] == char
    return is_char_at_i(min) != is_char_at_i(max)

if __name__ == "__main__":
    print("asd")
    inputs = read_input()
    print("asd")
    valid_pws = sum(starmap(is_pw_valid, inputs))
    valid_pws_v2 = sum(starmap(is_pw_valid_v2, inputs))
    print(valid_pws, valid_pws_v2)