import sys
from math import prod

nums = sorted(map(int, sys.stdin.read().split("\n")))

def get_combined_nums():
    for large_num in reversed(nums):
        i = 0
        while (nums[i] + large_num) < 2020:
            i += 1
        if nums[i]+large_num == 2020:
            return (nums[i], large_num)

def get_combined_v2():
    for num1 in nums:
        for num2 in nums:
            for num3 in nums:
                if num1 + num2 + num3 == 2020:
                    return (num1, num2, num3)


if __name__ == "__main__":
    print(prod(get_combined_nums()))
    print(prod(get_combined_v2()))
