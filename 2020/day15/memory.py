from typing import Generator, List

def sequence(init: List[int]) -> Generator[int, None, None]:
    memory = dict((val, (turn+1,)) for turn, val in enumerate(init))
    num = init[-1]
    turn_num = len(init) + 1
    while True:
        prev_last_turn, *last_turn = memory[num]
        if last_turn:
            [last_turn] = last_turn
            num = last_turn - prev_last_turn
        else:
            num = 0
        memory[num] = (memory[num][-1], turn_num) if num in memory else (turn_num,)
        turn_num += 1
        yield num


def main(raw_input: str, nums_of_interest):
    start_nums = [int(num) for num in raw_input.split(",")]
    nums_of_interest = sorted(nums_of_interest)

    generator = sequence(start_nums)
    for n in range(len(start_nums)+1, nums_of_interest[-1] + len(start_nums)):
        nth = next(generator)
        if n == nums_of_interest[0]:
            nums_of_interest.pop(0)
            print(f"number {n}: {nth}")
        if not nums_of_interest:
            break

if __name__ == "__main__":
    main("11,18,0,20,1,7,16", nums_of_interest=[2020, 30_000_000])