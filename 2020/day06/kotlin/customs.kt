import java.io.File
import kotlin.collections.Set

typealias Reducer<T> = (Set<T>, Set<T>) -> Set<T>
typealias Answer = List<Set<Char>>

fun getAnswers(): List<Answer> = File("answers.txt")
    .readText()
    .split("\n\n")
    .map({it.split("\n")})
    .map({it.map({it.toCharArray().toSet()})})

fun count(answers: List<Answer>, reducer: Reducer<Char>): Int = answers
    .map({it.reduce(reducer).size})
    .sum()

fun main() {
    var answers = getAnswers()
    println(count(answers, Set<Char>::union)) // part 1
    println(count(answers, Set<Char>::intersect)) // part 2
}