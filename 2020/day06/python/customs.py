import sys
from typing import Callable, Iterable, List, Set
from functools import reduce

raw_groups = sys.stdin.read().split("\n\n")

def get_answers(group_string) -> List[Set[str]]:
    return list(map(set, group_string.split("\n")))

def count_answers(group_answers: Iterable[Set[str]], reducer: Callable) -> int:
    return len(reduce(reducer, group_answers))

if __name__ == "__main__":
    answers = list(map(get_answers, raw_groups))
    print(sum(count_answers(a, set.union) for a in answers))        # part 1
    print(sum(count_answers(a, set.intersection) for a in answers)) # part 2