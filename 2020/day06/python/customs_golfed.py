import sys
from functools import reduce

answers = [list(map(set, group.split("\n")))
           for group in sys.stdin.read().split("\n\n")]

print(sum((len(reduce(set.union, a)) for a in answers)))        # part 1
print(sum((len(reduce(set.intersection, a)) for a in answers))) # part 2