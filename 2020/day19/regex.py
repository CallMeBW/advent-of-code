import re
import sys
from typing import Iterable, List, Tuple


class Matcher:
    RULE_PATTERN = r"^(\d+): (.*?)$"
    subs = dict()
    literals = dict()

    def __init__(self, rules: Iterable):
        for rule in rules:
            self.add_rule(rule)

    def matches(self, string: Iterable, rule_ids: Tuple) -> bool:
        if not rule_ids or not string:
            return not rule_ids and not string
        head_rule_id, tail_rules = rule_ids[0], rule_ids[1:]
        head_char, *tail_substring = string
        tail_substring = tuple(tail_substring)
        if head_rule_id in self.literals:
            head_matches = self.literals[head_rule_id] == head_char
            return head_matches and self.matches(tail_substring, tail_rules)
        return any(self.matches(string, (*sub, *tail_rules)) for sub in self.subs[head_rule_id])

    def add_rule(self, raw_rule: str):
        rule_id, rule = re.match(self.RULE_PATTERN, raw_rule).groups()
        if '"' in rule:
            self.literals[rule_id] = rule[1:-1]
            return
        self.subs[rule_id] = set(tuple(seq.split(" ")) for seq in rule.split(' | '))

def main(raw_rules: List[str], messages: List[str]):
    m = Matcher(raw_rules)
    count_matches = lambda: sum((m.matches(msg, ['0']) for msg in messages))
    # part 1
    print(count_matches())

    # part 2
    m.add_rule("8: 42 | 42 8")
    m.add_rule("11: 42 31 | 42 11 31")
    print(count_matches())


if __name__ == "__main__":
    main(*[block.split("\n") for block in sys.stdin.read().split("\n\n")])
