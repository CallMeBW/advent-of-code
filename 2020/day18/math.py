import sys
import re

FLAT_PARENTHESIS = r"\([0-9\s+*]+\)"
DIGITS = r"\d+"
DIGITS_CHAIN = r"((?:\d+ \+ )+\d+)"

def simple_maths(expression: str) -> int:
    expression, n_subs = re.subn(DIGITS, lambda m: m.group(0) + ')',expression)
    return eval(('('*n_subs) + expression)

def higher_maths(expression: str) -> int:
    return eval(re.sub(DIGITS_CHAIN, lambda m: '('+m.group(0)+')', expression))

def evaluate(expression: str, maths) -> int:
    if '(' in expression:
        expression = re.sub(FLAT_PARENTHESIS, lambda m: str(maths(m.group(0)[1:-1])), expression)
        return evaluate(expression, maths)
    return maths(expression)

if __name__ == "__main__":
    lines = sys.stdin.read().split("\n")
    print(sum([evaluate(line, simple_maths) for line in lines]))
    print(sum([evaluate(line, higher_maths) for line in lines]))