import sys
import re

def parse_passport(raw_string):
    key_values = raw_string.replace("\n", " ").split(" ")
    return dict(s.split(':', 1) for s in key_values)


def validate_eye_color(s):
    assert s in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"], "invalid eye color"


def validate_haircolor(s):
    assert re.match(r"^#[0-9a-f]{6}$", s) is not None, "haircolor does not match regex"


def assert_range(num, _min, _max):
    assert num >= _min and num <= _max, f"value not within {_min}, {_max}"


def validate_num_range(s, _min, _max, length=None):
    if length is not None:
        assert(len(s) == length), "length"
    assert s.isdigit(), "not a valid number"
    num = int(s)
    assert_range(num, _min, _max)


def validate_pid(s):
    assert len(s) == 9, "pid is not a 9-digit number"
    assert s.isdigit(), "not a digit"


def validate_height(s):
    m = re.search(r"^(?P<h>^\d*)(?P<unit>cm|in)$", s)
    assert m, "height has to be followed by cm or in"
    height, unit = (int(m.group("h")), m.group("unit"))
    if unit == "cm":
        assert_range(height, 150, 193)
    elif unit == "in":
        assert_range(height, 59, 76)


validators = {
    "byr": lambda s: validate_num_range(s, 1920, 2002, 4),
    "iyr": lambda s: validate_num_range(s, 2010, 2020, 4),
    "eyr": lambda s: validate_num_range(s, 2020, 2030, 4),
    "hgt": validate_height,
    "ecl": validate_eye_color,
    "hcl": validate_haircolor,
    "pid": validate_pid
}


def all_fields_present(passport):
    return all(field_name in passport.keys() for field_name in validators.keys())


def is_passport_valid(passport, print_invalid_reason=False):
    for field_name, validate in validators.items():
        try:
            validate(passport[field_name])
        except AssertionError as e:
            if print_invalid_reason:
                print(f"invalid {field_name}: '{passport[field_name]}' {e}")
            return False
    return True


if __name__ == "__main__":
    print_errors = False
    all_pp = map(parse_passport, sys.stdin.read().split("\n\n"))
    complete_pp = [pp for pp in all_pp if all_fields_present(pp)]
    valid_pp = [is_passport_valid(pp, print_errors) for pp in complete_pp]

    print(len(complete_pp)) # part 1
    print(sum(valid_pp))    # part 2