import sys
from typing import Tuple
import math

Instruction = Tuple[str, int]

def parse_instruction(line: str) -> Instruction:
    return (line[:1], int(line[1:]))

steering = { #x,y
    'N': (0, 1),
    'S': (0, -1),
    'W': (-1, 0),
    'E': (1, 0)
}

angle_to_dir = {
    0: 'E',
    90: 'S',
    180: 'W',
    270: 'N',
}

def rotate(x,y, degree):
    alpha = - math.radians(degree) # default rot. is counter-clockwise
    x_new = math.cos(alpha)*x - math.sin(alpha)*y
    y_new = math.sin(alpha)*x + math.cos(alpha)*y
    return (int(round(x_new)), int(round(y_new)))

def ship_xy_part1(instructions):
    x= y=0
    angle = 0
    for cmd, val in instructions:
        x_incr= y_incr = 0
        if cmd in steering:
            x_incr, y_incr = steering[cmd]
        elif cmd == 'F':
            x_incr, y_incr = steering[angle_to_dir[angle]]
        elif cmd == 'R':
            angle = (angle + val) % 360
        elif cmd == 'L':
            angle = (angle - val) % 360
        x += x_incr * val
        y += y_incr * val
    return x,y

def ship_xy_part2(instructions):
    wp_x = 10
    wp_y = 1
    ship_x=0
    ship_y=0
    for cmd, val in instructions:
        if cmd in steering:
            x_incr, y_incr = steering[cmd]
            wp_x += x_incr * val
            wp_y += y_incr * val
        elif cmd == 'F':
            ship_x += wp_x * val
            ship_y += wp_y * val
        elif cmd == 'R':
            wp_x, wp_y = rotate(wp_x, wp_y, val)
        elif cmd == 'L':
            wp_x, wp_y = rotate(wp_x, wp_y, -val)
    return ship_x, ship_y

def distance(x,y):
    return abs(x)+ abs(y)

if __name__ == "__main__":
    instructions = list(map(parse_instruction, sys.stdin.readlines()))
    print(distance(*ship_xy_part1(instructions)))
    print(distance(*ship_xy_part2(instructions)))
