import sys
from typing import Iterable, Tuple

def get_input() -> Tuple[int, Iterable[int]]:
    departure, busses = sys.stdin.read().split("\n")
    busses = (int(bus_id) for bus_id in busses.split(",") if bus_id != 'x')
    return int(departure), busses

def waiting_time(arrival: int, bus_id: int) -> int:
    return (arrival//bus_id+1)*bus_id-arrival

def earliest_departure(arrival: int, busses: Iterable[int]):
    waiting_times = ((bus, waiting_time(arrival, bus)) for bus in busses)
    return min(waiting_times, key=lambda tupl: tupl[1])

if __name__ == "__main__":
    waiting_time, bus_id = earliest_departure(*get_input())
    print(waiting_time * bus_id)  # part 1
