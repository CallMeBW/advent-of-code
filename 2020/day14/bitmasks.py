""" Day 14, Advent of Code: applying bit masks """
import re
import sys
import math
from dataclasses import dataclass
from itertools import chain, combinations
from typing import Iterable, List, Tuple

INSTRUCTION_PATTERN = r"^mem\[(\d+)\] = (\d+)$"


@dataclass
class Mask:
    """ given a string mask with chars {'X','1','0'}, populates various bit masks"""

    def __init__(self, mask: str):
        self.original = mask
        self.one_mask = int(mask.replace('X', '0'), base=2)
        self.x_mask = int(mask.replace('1', '0').replace('X', '1'), base=2)
        self.x_sig_indices = tuple(
            i for i, x in enumerate(reversed(mask)) if x == "X")


def combine_all_lengths(iterable: Iterable) -> Iterable:
    """ expands iterable <1,3> to iterable <(,),(1,),(3,),(1,3)> """
    all_lengths = range(len(iterable) + 1)
    return chain.from_iterable(combinations(iterable, l) for l in all_lengths)


def parse_instructions(raw_lines: List[str]) -> List[Tuple[Mask, int, int]]:
    """ Creates a list of instructions with tuples (mask, address, value) """
    mask = None
    instructions = []
    for raw_line in raw_lines:
        if (m := re.match(INSTRUCTION_PATTERN, raw_line)) is None:
            mask = Mask(raw_line[7:])
            continue
        instructions.append((mask, *(map(int, m.groups()))))
    return instructions


def decode_val(value: int, mask: Mask) -> int:
    """ decodes the value to be written to memory based on the given mask """
    return (mask.x_mask & value) | mask.one_mask


def decode_addr(orig_address: int, mask: Mask) -> Iterable[int]:
    """ creates a list of addresses based on the original address and the applied mask"""
    floating_indices = [int(math.pow(2, i)) for i in mask.x_sig_indices]
    combos = map(sum, combine_all_lengths(floating_indices))
    address_mask = (~mask.x_mask & orig_address) | mask.one_mask
    return (address_mask | num for num in combos)


def get_memory(instructions, addr_decoder=(lambda a, _: [a]), value_decoder=(lambda v, _: v)):
    """ fills a memory dict by address:value according to instructions """
    memory = dict()
    for mask, address, value in instructions:
        decoded_addr = addr_decoder(address, mask)
        decoded_val = value_decoder(value, mask)
        memory.update((addr, decoded_val) for addr in decoded_addr)
    return memory


def main():
    """ prints the answers to part 1 and 2 of advent of code day 14"""
    instructions = parse_instructions(sys.stdin.read().split("\n"))
    mem1 = get_memory(instructions, value_decoder=decode_val)
    mem2 = get_memory(instructions, addr_decoder=decode_addr)
    print(sum(mem1.values()))  # part 1
    print(sum(mem2.values()))  # part 2


if __name__ == "__main__":
    main()
