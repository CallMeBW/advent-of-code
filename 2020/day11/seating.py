import sys

def is_occupied(char: str)-> bool:
    return char == '#'

def is_empty(char: str)-> bool:
    return char == 'L'

def occupied_seats(row, slice_, grid):
    if row < 0 or row >= len(grid):
        return 0
    x = sum([char == '#' for char in grid[row][slice_]])
    return x

def count_total_occ_seats(grid):
    return sum([occupied_seats(row, slice(0,len(grid[row])), grid) for row in range(0, len(grid))])


def occupied_seats_around(row, col, grid):
    max_ind = len(grid[0])
    slice_ = slice(max(col-1,0), min(col+2, max_ind))
    return sum([occupied_seats(i, slice_, grid) for i in range(row-1,row+2)]) - is_occupied(grid[row][col])

def simulate_round(grid):
    new_grid = []
    did_change = False
    for i in range(0,len(grid)):
        new_grid.append("")
        for j in range(0, len(grid[0])):
            occupied_around = occupied_seats_around(i,j,grid)
            if is_empty(grid[i][j]) and occupied_around == 0:
                new_grid[i]+='#'
                did_change = True
            elif occupied_around >= 4 and is_occupied(grid[i][j]):
                new_grid[i]+='L'
                did_change = True
            else:
                new_grid[i]+=grid[i][j]
    return new_grid, did_change

if __name__ == "__main__":
    layout = sys.stdin.read().split("\n")
    changed=True
    while changed:
        layout, changed = simulate_round(layout)
    print(count_total_occ_seats(layout))
